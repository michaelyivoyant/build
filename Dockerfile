FROM node:10-alpine AS build

WORKDIR /usr/src/app

COPY . .
RUN echo ${KUBERNETES_VERSION}
RUN echo $KUBERNETES_VERSION

EXPOSE 3000

RUN npm install -g serve

CMD ["serve", "-l", "3000", "-s", "dashboard"]